## What is this?
- A basic 5 day weather forecast single page app.

## Stack
- React: For building the performant resuable UI interfaces.
- Redux: State container.
- Express: Node server for serving static assets and initial HTML, can be extended as an API server.
- Babel: Transpiling ES6 code to browser compatible ES5 code.
- moment: Date/time handling
- webpack: Build & Modile loader
- jest: Unit testing framework.
- Bootstrap: UI toolkit for building layouts

## Setup 
  * node v6.11.2
  * npm 3.10.10
  * MacOs

## npm tasks

* `start` - Builds and starts the server
* `jest`  - runs the unit test

## Getting Started

**1. Clone the project

```
git clone git@bitbucket.org:atheeqpasha/weather-app.git
```

**2. You can start by navigating to the root folder on your local machine by running:**

```bash
cd weather-app
```

**3. Install all of the dependencies:**

```bash
npm install
```

**4. Start to run it:**
```bash
npm start
```
Now the app should be running at http://localhost:3000

**5. Run the tests:**
```bash
npm test
```

## App Structure

Here is the structure of the app.

```
.
|
├── dist                              # Webpack bundled files will be placed into it
│   └── bundle.js                     # bundled js+css+vendor file.
|
├── src                               # App Source code
|   |
|   |__ js
|   |   |__ actions                   # redux actions
|   |   |__ api                       # API client for making promise based networks
|   |   |__ components                # Reusable components
|   |   |__ redux                     # Folder containing app reducer and store configuration
│   |   |__ index.js                  # Webpack app entry file
│   |   |── server.js                 # Express server
|   |
|   |__ styles
|   |  |__ main.scss                  # App Styles
|   |
|   |__ tests                         # tests folder
|   |
|   |__ index.html                    # Initial HTML
|       
└── package.json                      # NPM denpendencies
|__ webpack.config                    # webpack entrypoint for building 
```

## Deployment
The app is deployed on Heroku running at 
```
https://weather-node-8080.herokuapp.com/
```

## What could be done with more time
- Integrate Weather icons
- Show weather forecast about the complete day
- Adding Loading indicator on search
- Adding Error states when the API fails