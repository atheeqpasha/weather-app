const express = require('express');
const app = express();
const path = require('path');
const PORT = process.env.PORT || 3000

//serve up static files
app.use('/dist', express.static('dist'));

app.get('/', function (request, response) {
  response.sendFile(path.resolve(__dirname, '..', '../src', 'index.html'))
});

app.listen(PORT, function(err) {
  if (err) {
    console.log(err);
    return;
  }
  console.log("Listening on port " + PORT);
});
