import promisePolyfill from 'es6-promise';
import 'isomorphic-fetch';

promisePolyfill.polyfill();

const API_KEY = '7354ab51d8f0ca0d35772aa5fb1dfb4f';

function getWeatherForecast(location) {
  return fetch(`http://api.openweathermap.org/data/2.5/forecast?APPID=${API_KEY}&units=metric&q=${location}`)
    .then(response => response.json());
};

export default {
  getWeatherForecast,
};
