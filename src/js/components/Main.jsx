import React from 'react';
import moment from 'moment'
import _ from "lodash";

class TodayForecast extends React.Component {
  render() {
    let { city, day, weather } = this.props;
    let m = moment(day);
    let currentHour = weather[0];

    return (
      <div className="today forecast">
        <div className="forecast-header">
          <div className="day">{ m.format('dddd') }</div>
          <div className="date">{ m.format('D MMM') }</div>
        </div>
        <div className="forecast-content">
          <div className="location">{ city.name }</div>
          <div className="degree">
            <div className="num">{ _.round(currentHour.main.temp, 1) }<sup>o</sup>C</div>
          </div>
          <span>C: { currentHour.clouds.all }%</span>
          <span>W: { _.round(currentHour.wind.speed * 3.6, 1) } km/h</span>
          <span>H: { _.round(currentHour.main.humidity, 1) }%</span>
        </div>
      </div>
    );
  }
};

class DayForecast extends React.Component {

  render() {
    let m = moment(this.props.day);
    let currentHour = this.props.weather[0];

    return (
      <div className="forecast">
        <div className="forecast-header">
          <div className="day">{ m.format('dddd') }</div>
        </div>
        <div className="forecast-content">
          <div className="degree">{ _.round(currentHour.main.temp, 1) }<sup>o</sup>C</div>
          <small>min :{ _.round(currentHour.main.temp_min, 1)}<sup>o</sup>C</small><br/>
          <small>max :{ _.round(currentHour.main.temp_max, 1)}<sup>o</sup>C</small>
        </div>
      </div>
    );
  }
}

export default class Main extends React.Component {

    constructor (props) {
        super(props);
    }

    groupByDays (list) {
      const days = new Map();
      for(let i of list) {
        const day = moment(i.dt * 1000).format("YYYY-MM-DD");
        if( !days[day] ) days[day] = []
        days[day].push(i)
      }
      return days;
    }

    render() {
        let days = this.groupByDays(this.props.forecast.list);
        let city = this.props.forecast.city;
        return (
          <main>
            <div className="container">
              <div className="forecast-container">
                { Object.keys(days).map((day, index) => (
                  index === 0 ? <TodayForecast key={day} day={day} city={city} weather={days[day] }/> : <DayForecast key={day} day={day} weather={days[day]}/> 
                ))}
              </div>
            </div>
          </main>
        );
    }
};
