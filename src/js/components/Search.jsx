import React from 'react';

export default class Search extends React.Component {
    constructor (props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit () {
      let location = this.refs.input.value.trim();
      this.refs.input.value = "";
      this.props.search(location);
    }

    render() {
        return (
          <div className="search">
              <div className="container">
                <div className="find-location">
                  <input type="text" placeholder="Find by location..." ref="input"/>
                  <div className="submit" onClick={ this.handleSubmit } >Find</div>
                </div>
              </div>
          </div>
        );
    }
}
