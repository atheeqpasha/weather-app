import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getWeatherForecast } from '../actions';

import Search from './Search';
import Main from './Main';
  
import "../../styles/main.scss";

class Home extends Component {

  propTypes: {
    data: PropTypes.object,
    error: PropTypes.object,
    isFetching: PropTypes.bool,
    getWeatherForecast: React.PropTypes.func
  }

  constructor (props) {
    super(props);
    this.search = this.search.bind(this);
  } 

  componentDidMount () {
    this.props.getWeatherForecast('bangalore');
  }

  search (location) {
    this.props.getWeatherForecast(location);
  }

  render() {
    return (
      <div className="site-content">
        <Search search={ this.search } />
        {this.props.forecast ? <Main forecast={this.props.forecast} /> : null}
      </div> 
    )
  }
};

// Extract the props we want to connect from the current store state
const mapStateToProps = (state) => ({
  isFetching: state.isFetching,
  forecast:   state.data,
  error:      state.error
});

// Add dispatchers to the component props for fetching the data _client side_
const mapDispatchToProps = (dispatch) => {  
  return { getWeatherForecast: (location) => dispatch(getWeatherForecast(location)) }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);