import React from 'react';
import ReactDOM from 'react-dom';
import Home from "./components/Home";
import {Provider} from 'react-redux';
import store from './redux/store';

import 'jquery';
import 'bootstrap-loader';

ReactDOM.render(
  <Provider store={store}>
    <Home />
  </Provider>,
  document.getElementById('root')
);