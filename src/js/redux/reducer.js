import _ from 'lodash';

import {
  ACTION_START,
  ACTION_ERROR,
  ACTION_SUCCESS
} from '../actions';

const initialState = {
  isFetching: false,
  error: null,
  data: null
};

const actionsMap = {
  // Async action
  [ACTION_START]: (state) => {
    return _.assign({}, state, { isFetching: true });
  },

  [ACTION_ERROR]: (state, action) => {
    return _.assign({}, state, { error: action.data, isFetching: false });
  },

  [ACTION_SUCCESS]: (state, action) => {
    return _.assign({}, state, { data: action.data, isFetching: false });
  }
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
};
