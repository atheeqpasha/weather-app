import api from '../api/index';

export const ACTION_START = 'ACTION_START';
export const ACTION_ERROR = 'ACTION_ERROR';
export const ACTION_SUCCESS = 'ACTION_SUCCESS';

export const start = () => ({
  type: ACTION_START
})

export const success = (data) => ({
  type: ACTION_SUCCESS,
  data,
})

export const error = (error) => ({
  type: ACTION_ERROR,
  error,
})

export function getWeatherForecast(location) {
  return function (dispatch) {
    dispatch(start());
    return api.getWeatherForecast(location)
      .then(data => dispatch(success(data)))
      .catch(error => dispatch(error(error)));
  };
};
