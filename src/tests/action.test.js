import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../js/actions';
import nock from 'nock';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const API_KEY = '7354ab51d8f0ca0d35772aa5fb1dfb4f';
const host = 'http://api.openweathermap.org';

describe('async actions', () => {

  const response = [{ id: '1', name: 'Welly' }];

  afterEach(() => {
    nock.cleanAll();
  })

  it('should have a type of ACTION_START', () => {
    expect(actions.start().type).toEqual('ACTION_START');
  });

  it('should create an action to add to start a request', () => {
    const expectedAction = {
      type: actions.ACTION_START
    }
    expect(actions.start()).toEqual(expectedAction);
  });

  it('should have a type of ACTION_ERROR', () => {
    expect(actions.error().type).toEqual('ACTION_ERROR');
  });

  it('should create an action to handle error response', () => {
    const error = "ERROR";
    const expectedAction = {
      type: actions.ACTION_ERROR,
      error
    }
    expect(actions.error(error)).toEqual(expectedAction);
  });

  it('should have a type of ACTION_SUCCESS', () => {
    expect(actions.success().type).toEqual('ACTION_SUCCESS');
  });

  it('should create an action to handle a successfull request', () => {
    const data = {"a": "b", "c" : []};

    const expectedAction = {
      type: actions.ACTION_SUCCESS,
      data
    }

    expect(actions.success(data)).toEqual(expectedAction);
  });

  test('creates ACTION_SUCCESS when fetching data has been done', () => {
    nock(host)
      .get('/data/2.5/forecast?APPID=7354ab51d8f0ca0d35772aa5fb1dfb4f&units=metric&q=bangalore')
      .reply(200, response);

    const expectedActions = [
        { type: actions.ACTION_START },
        { type: actions.ACTION_SUCCESS, data: response }
    ]

    const store = mockStore({ todos : [] });

    return store.dispatch(actions.getWeatherForecast('bangalore')).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    });

  });

});
