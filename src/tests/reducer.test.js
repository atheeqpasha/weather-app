import reducer from '../js/redux/reducer';
import * as actions from '../js/actions';

describe('home page data reducer', () => {
  test('should return the initial state', () => {
    expect(
      reducer(undefined, {}),
    ).toEqual({
      isFetching: false,
      error: null,
      data: null
    });
  });

  test('should handle ACTION_START', () => {
    expect(
      reducer(undefined, {
        type: actions.ACTION_START,
        error: null,
        data: null,
      }),
    ).toEqual({
      isFetching: true,
      error: null,
      data: null
    });
  });

  test('should handle ACTION_ERROR', () => {
    expect(
      reducer(undefined, {
        type: actions.ACTION_ERROR,
        error: undefined
      }),
    ).toEqual({
      isFetching: false,
      error:  undefined,
      data: null,
    });
  });

  test('should handle USERS_SUCCESS', () => {
    expect(
      reducer(undefined, {
        type: actions.ACTION_SUCCESS,
        data: [{ id: '1', name: 'Welly' }],
      }),
    ).toEqual({
      isFetching: false,
      error:  null,
      data: [{ id: '1', name: 'Welly' }]
    });
  });

});